package com.example.ficheboukellal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ModifierIdentiteActivity extends AppCompatActivity {

    // Déclaration des EditText
    private EditText etNom, etPrenom, etTelephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_identite);

        // Initialisation des EditText
        etNom = findViewById(R.id.etNom);
        etPrenom = findViewById(R.id.etPrenom);
        etTelephone = findViewById(R.id.etTelephone);

        // Initialisation des boutons
        // Déclaration des boutons
        Button btnValider = findViewById(R.id.btnValider);
        Button btnAnnuler = findViewById(R.id.btnAnnuler);

        // Gestionnaire de clic pour le bouton Valider
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validerModifications();
            }
        });

        // Gestionnaire de clic pour le bouton Annuler
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                annulerModifications();
            }
        });

        // Récupération des valeurs actuelles des champs d'identité
        String nom = getIntent().getStringExtra("nom");
        String prenom = getIntent().getStringExtra("prenom");
        String telephone = getIntent().getStringExtra("telephone");

        // Affichage des valeurs actuelles dans les EditText
        etNom.setText(nom);
        etPrenom.setText(prenom);
        etTelephone.setText(telephone);
    }

    // Méthode pour valider les modifications et renvoyer les nouvelles valeurs à l'activité principale
    private void validerModifications() {
        String nom = etNom.getText().toString();
        String prenom = etPrenom.getText().toString();
        String telephone = etTelephone.getText().toString();

        // Création de l'intent avec les nouvelles valeurs
        Intent intent = new Intent();
        intent.putExtra("nom", nom);
        intent.putExtra("prenom", prenom);
        intent.putExtra("telephone", telephone);

        // Résultat OK avec l'intent contenant les nouvelles valeurs
        setResult(RESULT_OK, intent);
        finish();
    }

    // Méthode pour annuler les modifications et renvoyer un résultat annulé à l'activité principale
    private void annulerModifications() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

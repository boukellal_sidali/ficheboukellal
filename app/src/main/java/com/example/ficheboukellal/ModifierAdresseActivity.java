package com.example.ficheboukellal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ModifierAdresseActivity extends AppCompatActivity {

    // Déclaration des EditText
    private EditText etNumero, etRue, etCodePostal, etVille;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_adresse);

        // Initialisation des EditText
        etNumero = findViewById(R.id.etNumero);
        etRue = findViewById(R.id.etRue);
        etCodePostal = findViewById(R.id.etCodePostal);
        etVille = findViewById(R.id.etVille);

        // Initialisation des boutons
        // Déclaration des boutons
        Button btnValider = findViewById(R.id.btnValider);
        Button btnAnnuler = findViewById(R.id.btnAnnuler);

        // Gestionnaire de clic pour le bouton Valider
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validerModifications();
            }
        });

        // Gestionnaire de clic pour le bouton Annuler
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                annulerModifications();
            }
        });

        // Récupération des valeurs actuelles des champs d'adresse
        String numero = getIntent().getStringExtra("numero");
        String rue = getIntent().getStringExtra("rue");
        String codePostal = getIntent().getStringExtra("codePostal");
        String ville = getIntent().getStringExtra("ville");

        // Affichage des valeurs actuelles dans les EditText
        etNumero.setText(numero);
        etRue.setText(rue);
        etCodePostal.setText(codePostal);
        etVille.setText(ville);
    }

    // Méthode pour valider les modifications et renvoyer les nouvelles valeurs à l'activité principale
    private void validerModifications() {
        String numero = etNumero.getText().toString();
        String rue = etRue.getText().toString();
        String codePostal = etCodePostal.getText().toString();
        String ville = etVille.getText().toString();

        // Création de l'intent avec les nouvelles valeurs
        Intent intent = new Intent();
        intent.putExtra("numero", numero);
        intent.putExtra("rue", rue);
        intent.putExtra("codePostal", codePostal);
        intent.putExtra("ville", ville);

        // Résultat OK avec l'intent contenant les nouvelles valeurs
        setResult(RESULT_OK, intent);
        finish();
    }

    // Méthode pour annuler les modifications et renvoyer un résultat annulé à l'activité principale
    private void annulerModifications() {
        setResult(RESULT_CANCELED);
        finish();
    }
}

package com.example.ficheboukellal;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Déclaration des TextViews
    private TextView tvNom, tvPrenom, tvTelephone, tvNumero, tvRue, tvCodePostal, tvVille;

    // Variables pour stocker les valeurs des champs
    private String nom, prenom, telephone, numero, rue, codePostal, ville;

    // Déclaration des launchers pour les activités de modification
    private ActivityResultLauncher<Intent> modifierIdentiteLauncher;
    private ActivityResultLauncher<Intent> modifierAdresseLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialisation des TextViews
        tvNom = findViewById(R.id.tvNom);
        tvPrenom = findViewById(R.id.tvPrenom);
        tvTelephone = findViewById(R.id.tvTelephone);
        tvNumero = findViewById(R.id.tvNumero);
        tvRue = findViewById(R.id.tvRue);
        tvCodePostal = findViewById(R.id.tvCodePostal);
        tvVille = findViewById(R.id.tvVille);

        // Initialisation des boutons
        // Déclaration des boutons
        Button btnModifierIdentite = findViewById(R.id.btnModifierIdentite);
        Button btnModifierAdresse = findViewById(R.id.btnModifierAdresse);

        // Gestionnaire de clic pour le bouton Modifier Identité
        btnModifierIdentite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchModifierIdentiteActivity();
            }
        });

        // Gestionnaire de clic pour le bouton Modifier Adresse
        btnModifierAdresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchModifierAdresseActivity();
            }
        });

        // Initialisation du launcher pour l'activité de modification de l'identité
        modifierIdentiteLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent intent = result.getData();
                        if (intent != null) {
                            // Récupération des données modifiées
                            nom = intent.getStringExtra("nom");
                            prenom = intent.getStringExtra("prenom");
                            telephone = intent.getStringExtra("telephone");
                            // Mise à jour des champs d'identité
                            updateIdentiteFields();
                        }
                    }
                });

        // Initialisation du launcher pour l'activité de modification de l'adresse
        modifierAdresseLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent intent = result.getData();
                        if (intent != null) {
                            // Récupération des données modifiées
                            numero = intent.getStringExtra("numero");
                            rue = intent.getStringExtra("rue");
                            codePostal = intent.getStringExtra("codePostal");
                            ville = intent.getStringExtra("ville");
                            // Mise à jour des champs d'adresse
                            updateAdresseFields();
                        }
                    }
                });
    }

    // Méthode pour lancer l'activité de modification de l'identité
    private void launchModifierIdentiteActivity() {
        Intent intent = new Intent(MainActivity.this, ModifierIdentiteActivity.class);
        // Passage des valeurs actuelles des champs d'identité à l'activité de modification
        intent.putExtra("nom", nom);
        intent.putExtra("prenom", prenom);
        intent.putExtra("telephone", telephone);
        modifierIdentiteLauncher.launch(intent);
    }

    // Méthode pour lancer l'activité de modification de l'adresse
    private void launchModifierAdresseActivity() {
        Intent intent = new Intent(MainActivity.this, ModifierAdresseActivity.class);
        // Passage des valeurs actuelles des champs d'adresse à l'activité de modification
        intent.putExtra("numero", numero);
        intent.putExtra("rue", rue);
        intent.putExtra("codePostal", codePostal);
        intent.putExtra("ville", ville);
        modifierAdresseLauncher.launch(intent);
    }

    // Méthode pour mettre à jour les champs d'identité
    private void updateIdentiteFields() {
        tvNom.setText(nom);
        tvPrenom.setText(prenom);
        tvTelephone.setText(telephone);
    }

    // Méthode pour mettre à jour les champs d'adresse
    private void updateAdresseFields() {
        tvNumero.setText(numero);
        tvRue.setText(rue);
        tvCodePostal.setText(codePostal);
        tvVille.setText(ville);
    }
}
